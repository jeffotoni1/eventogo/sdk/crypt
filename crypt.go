package crypt

import (
	// #nosec
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"

	// #nosec
	"math/rand"
	"strconv"
	"time"

	"strings"

	"github.com/google/uuid"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
)

var SHA1_SALT = "x66.iutvieu$.867ury.m987$#894x42."

func GSha1(key string) string {
	if len(key) == 0 {
		return ""
	}
	data := []byte(fmts.ConcatStr(key, SHA1_SALT))
	// #nosec
	sh1 := sha1.Sum(data)
	return fmt.Sprintf("%x", sh1)
}

func Random() (string, error) {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	// #nosec
	md5str, err := Md5(strconv.Itoa(r.Intn(1000000) + r.Intn(100000)))
	if err != nil {
		return "", err
	}
	return md5str, nil
}

func GenMsgID() (string, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}
	correlationID := strings.ToUpper(strings.Replace(id.String(), "-", "", -1))[:24]
	return correlationID, nil
}

func Md5(text string) (string, error) {
	if len(text) == 0 {
		return "", errors.New("text cannot be empty")
	}
	// #nosec
	hash := md5.New()
	_, err := hash.Write([]byte(text))
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}
