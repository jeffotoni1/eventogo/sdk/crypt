package crypt

import "testing"

func TestRandom(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{name: "test_random", want: "", wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Random()
			if (err != nil) != tt.wantErr {
				t.Errorf("Random() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(tt.want) == 0 {
				t.Log("Random()")
			} else if got != tt.want {
				t.Errorf("Random() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenMsgID(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{name: "test_genmsgid", want: "", wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenMsgID()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenMsgID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(tt.want) == 0 {
				t.Log("GenMsgID()")
			} else if got != tt.want {
				t.Errorf("GenMsgID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMd5(t *testing.T) {
	type args struct {
		text string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{name: "test_md5", want: "e8961ed73a43314af2cce47a4f5b65db", wantErr: false, args: args{text: "jeffotoni"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Md5(tt.args.text)
			if (err != nil) != tt.wantErr {
				t.Errorf("Md5(tt.args.text) error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(tt.want) == 0 {
				t.Log("Md5(tt.args.text)")
			} else if got != tt.want {
				t.Errorf("Md5(tt.args.text) = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGSha1(t *testing.T) {
	type args struct {
		text string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{name: "test_gsha1", want: "0ae8d2e2efef2d436996267fbd5aa243e196b118", wantErr: false, args: args{text: "jeffotoni"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GSha1(tt.args.text)
			if len(tt.want) == 0 {
				t.Logf("GSha1(tt.args.text) %s", got)
			} else if got != tt.want {
				t.Errorf("GSha1(tt.args.text) = %v, want %v", got, tt.want)
			}
		})
	}
}
